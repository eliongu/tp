echo "Machine name :$(hostnamectl | grep Static | cut -d " " -f4)"
echo "OS $(uname -r) and kernel version is $(source /etc/os-release && echo $NAME)"
echo "IP : $(ip a | grep -E '^2:.*' -A 2 | grep inet | tr -s ' '  | cut -d' ' -f3)"
echo "RAM : $(free -mh | grep -E '^Mem.*' | tr -s ' '  | cut -d' ' -f4) memory available on $(free -mh | grep -E '^Mem.*' | tr -s ' '  | cut -d' ' -f2) total memory"
echo "Disk : $(df -mh | grep -E '/$' | tr -s ' '  | cut -d' ' -f4) space left"
echo -e "Top 5 processes by RAM usage:\n$(while read super_line; do
        echo "- $super_line"
done <<< $(ps -eo comm --sort=-%mem | head -n5))"
echo -e "Listening ports :\n$(while read super_line; do
echo "- $super_line : $(ss -tuln | grep :[1-9] | tr -s " " | cut -d " " -f7)"
done <<< $(paste -d " " <(ss -tuln | grep :[1-9] | tr -s " " | cut -d " " -f5 | sed 's/.*://') <(ss -tuln | grep :[1-9] | tr -s " " | cut -d " " -f1)))"

echo -e "PATH directories :\n$(while read -r directory; do
    echo "- $directory"
done <<< $(echo $PATH | tr -s ":" '\n'))"

echo -e "Here is your random cat (jpg file) : $(curl https://api.thecatapi.com/v1/images/search -s | cut -d '"' -f8)"