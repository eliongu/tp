**TP 3**
-

🌞 S'assurer que le service sshd est démarré

~~~
[elio@localhost ~]$ sudo systemctl list-units -t service -a | grep sshd
  sshd-keygen@ecdsa.service              loaded    inactive dead    OpenSSH ecdsa Server Key Generation
  sshd-keygen@ed25519.service            loaded    inactive dead    OpenSSH ed25519 Server Key Generation
  sshd-keygen@rsa.service                loaded    inactive dead    OpenSSH rsa Server Key Generation
  sshd.service                           loaded    active   running OpenSSH server daemon
~~~

🌞 Analyser les processus liés au service SSH

```
[elio@localhost ~]$ ps -ef | grep ssh
root         693       1  0 10:28 ?        00:00:00 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups
root        1090     693  0 10:40 ?        00:00:00 sshd: elio [priv]
elio        1093    1090  0 10:40 ?        00:00:00 sshd: elio@pts/0
elio        1133    1094  0 10:49 pts/0    00:00:00 grep --color=auto ssh
```

🌞 Déterminer le port sur lequel écoute le service SSH

```
[elio@localhost ~]$ ss | grep ssh
tcp   ESTAB  0      52                        10.3.1.11:ssh           10.3.1.1:62278
```

🌞 Consulter les logs du service SSH

```
[elio@node1 ~]$ journalctl -xe -u sshd
~
~
~
~
Jan 29 10:28:20 localhost systemd[1]: Starting OpenSSH server daemon...
░░ Subject: A start job for unit sshd.service has begun execution
░░ Defined-By: systemd
░░ Support: https://access.redhat.com/support
░░
░░ A start job for unit sshd.service has begun execution.
░░
░░ The job identifier is 224.
Jan 29 10:28:20 localhost sshd[693]: main: sshd: ssh-rsa algorithm is disabled
Jan 29 10:28:20 localhost sshd[693]: Server listening on 0.0.0.0 port 22.
Jan 29 10:28:20 localhost sshd[693]: Server listening on :: port 22.
Jan 29 10:28:20 localhost systemd[1]: Started OpenSSH server daemon.
░░ Subject: A start job for unit sshd.service has finished successfully
░░ Defined-By: systemd
░░ Support: https://access.redhat.com/support
░░
░░ A start job for unit sshd.service has finished successfully.
░░
░░ The job identifier is 224.
Jan 29 10:40:06 localhost.localdomain sshd[1090]: main: sshd: ssh-rsa algorithm is disabled
Jan 29 10:40:06 localhost.localdomain sshd[1090]: Accepted publickey for elio from 10.3.1.1 port 62278 ssh2: RSA SHA256>
Jan 29 10:40:06 localhost.localdomain sshd[1090]: pam_unix(sshd:session): session opened for user elio(uid=1000) by (ui>
Jan 29 10:54:49 node1.tp3.b1 sshd[1149]: main: sshd: ssh-rsa algorithm is disabled
Jan 29 10:54:49 node1.tp3.b1 sshd[1149]: Accepted publickey for elio from 10.3.1.1 port 62407 ssh2: RSA SHA256:hKmGAcUP>
Jan 29 10:54:49 node1.tp3.b1 sshd[1149]: pam_unix(sshd:session): session opened for user elio(uid=1000) by (uid=0)
```
```
[elio@node1 ~]$ sudo tail -n 10 /var/log/dnf.log
2023-10-23T11:43:29+0200 DDEBUG /var/cache/dnf/appstream-25485261a76941d3/packages/gpm-libs-1.20.7-29.el9.x86_64.rpm removed
2023-10-23T11:43:29+0200 DDEBUG /var/cache/dnf/appstream-25485261a76941d3/packages/python-unversioned-command-3.9.16-1.el9_2.2.noarch.rpm removed
2023-10-23T11:43:29+0200 DDEBUG /var/cache/dnf/baseos-522ed8e2b2f761ff/packages/traceroute-2.1.0-16.el9.x86_64.rpm removed
2023-10-23T11:43:29+0200 DDEBUG /var/cache/dnf/appstream-25485261a76941d3/packages/libmaxminddb-1.5.2-3.el9.x86_64.rpm removed
2023-10-23T11:43:29+0200 DDEBUG /var/cache/dnf/appstream-25485261a76941d3/packages/nmap-7.91-12.el9.x86_64.rpm removed
2023-10-23T11:43:29+0200 DDEBUG /var/cache/dnf/appstream-25485261a76941d3/packages/bind-utils-9.16.23-11.el9_2.2.x86_64.rpm removed
2023-10-23T11:43:29+0200 DDEBUG /var/cache/dnf/appstream-25485261a76941d3/packages/libuv-1.42.0-1.el9.x86_64.rpm removed
2023-10-23T11:43:29+0200 DDEBUG /var/cache/dnf/appstream-25485261a76941d3/packages/vim-common-8.2.2637-20.el9_1.x86_64.rpm removed
2023-10-23T11:43:29+0200 DDEBUG /var/cache/dnf/baseos-522ed8e2b2f761ff/packages/dhcp-common-4.4.2-18.b1.el9.noarch.rpm removed
2023-10-23T11:43:29+0200 DDEBUG Plugins were unloaded.
```

🌞 Identifier le fichier de configuration du serveur SSH

```shell
[elio@node1 ~]$ ls /etc/ssh/
moduli      ssh_config.d  sshd_config.d       ssh_host_ecdsa_key.pub  ssh_host_ed25519_key.pub  ssh_host_rsa_key.pub
##ssh_config##  sshd_config
```

🌞 Modifier le fichier de conf
```
[elio@node1 ~]$ echo $RANDOM
15345
```

```
[elio@node1 ~]$ sudo cat /etc/ssh/sshd_config
#       $OpenBSD: sshd_config,v 1.104 2021/07/02 05:11:21 dtucker Exp $

# This is the sshd server system-wide configuration file.  See
# sshd_config(5) for more information.

# This sshd was compiled with PATH=/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin

# The strategy used for options in the default sshd_config shipped with
# OpenSSH is to specify options with their default value where
# possible, but leave them commented.  Uncommented options override the
# default value.

# To modify the system-wide sshd configuration, create a  *.conf  file under
#  /etc/ssh/sshd_config.d/  which will be automatically included below
Include /etc/ssh/sshd_config.d/*.conf

# If you want to change the port on a SELinux system, you have to tell
# SELinux about this change.
# semanage port -a -t ssh_port_t -p tcp #PORTNUMBER
#
Port 15345
#AddressFamily any
#ListenAddress 0.0.0.0
#ListenAddress ::

#HostKey /etc/ssh/ssh_host_rsa_key
#HostKey /etc/ssh/ssh_host_ecdsa_key
#HostKey /etc/ssh/ssh_host_ed25519_key

# Ciphers and keying
#RekeyLimit default none

# Logging
#SyslogFacility AUTH
#LogLevel INFO

# Authentication:

#LoginGraceTime 2m
#PermitRootLogin prohibit-password
#StrictModes yes
#MaxAuthTries 6
#MaxSessions 10

#PubkeyAuthentication yes

# The default is to check both .ssh/authorized_keys and .ssh/authorized_keys2
# but this is overridden so installations will only check .ssh/authorized_keys
AuthorizedKeysFile      .ssh/authorized_keys

#AuthorizedPrincipalsFile none

#AuthorizedKeysCommand none
#AuthorizedKeysCommandUser nobody

# For this to work you will also need host keys in /etc/ssh/ssh_known_hosts
#HostbasedAuthentication no
# Change to yes if you don't trust ~/.ssh/known_hosts for
# HostbasedAuthentication
#IgnoreUserKnownHosts no
# Don't read the user's ~/.rhosts and ~/.shosts files
#IgnoreRhosts yes

# To disable tunneled clear text passwords, change to no here!
#PasswordAuthentication yes
#PermitEmptyPasswords no

# Change to no to disable s/key passwords
#KbdInteractiveAuthentication yes

# Kerberos options
#KerberosAuthentication no
#KerberosOrLocalPasswd yes
#KerberosTicketCleanup yes
#KerberosGetAFSToken no
#KerberosUseKuserok yes

# GSSAPI options
#GSSAPIAuthentication no
#GSSAPICleanupCredentials yes
#GSSAPIStrictAcceptorCheck yes
#GSSAPIKeyExchange no
#GSSAPIEnablek5users no

# Set this to 'yes' to enable PAM authentication, account processing,
# and session processing. If this is enabled, PAM authentication will
# be allowed through the KbdInteractiveAuthentication and
# PasswordAuthentication.  Depending on your PAM configuration,
# PAM authentication via KbdInteractiveAuthentication may bypass
# the setting of "PermitRootLogin without-password".
# If you just want the PAM account and session checks to run without
# PAM authentication, then enable this but set PasswordAuthentication
# and KbdInteractiveAuthentication to 'no'.
# WARNING: 'UsePAM no' is not supported in RHEL and may cause several
# problems.
#UsePAM no

#AllowAgentForwarding yes
#AllowTcpForwarding yes
#GatewayPorts no
#X11Forwarding no
#X11DisplayOffset 10
#X11UseLocalhost yes
#PermitTTY yes
#PrintMotd yes
#PrintLastLog yes
#TCPKeepAlive yes
#PermitUserEnvironment no
#Compression delayed
#ClientAliveInterval 0
#ClientAliveCountMax 3
#UseDNS no
#PidFile /var/run/sshd.pid
#MaxStartups 10:30:100
#PermitTunnel no
#ChrootDirectory none
#VersionAddendum none

# no default banner path
#Banner none

# override default of no subsystems
Subsystem       sftp    /usr/libexec/openssh/sftp-server

# Example of overriding settings on a per-user basis
#Match User anoncvs
#       X11Forwarding no
#       AllowTcpForwarding no
#       PermitTTY no
#       ForceCommand cvs server
```
```
[elio@node1 ~]$ sudo firewall-cmd --list-all | grep port
[sudo] password for elio:
  ports: 15345/tcp
```

🌞 Redémarrer le service 

```
[elio@node1 ~]$ sudo systemctl restart firewalld
[sudo] password for elio:
[elio@node1 ~]$
```

🌞 Effectuer une connexion SSH sur le nouveau port
```
PS C:\Users\elion> ssh -p 15345 elio@10.3.1.11
Last login: Mon Jan 29 11:50:52 2024
[elio@localhost ~]$
```

🌞 Installer le serveur NGINX

```shell
[elio@localhost ~]$ sudo dnf install nginx
[sudo] password for elio:
Last metadata expiration check: 0:24:32 ago on Mon 29 Jan 2024 11:38:00 AM CET.
Dependencies resolved.
========================================================================================================================
 Package                         Architecture         Version                             Repository               Size
========================================================================================================================
Installing:
 nginx                           x86_64               1:1.20.1-14.el9_2.1                 appstream                36 k
Installing dependencies:
 nginx-core                      x86_64               1:1.20.1-14.el9_2.1                 appstream               565 k
 nginx-filesystem                noarch               1:1.20.1-14.el9_2.1                 appstream               8.5 k
 rocky-logos-httpd               noarch               90.14-2.el9                         appstream                24 k

Transaction Summary
========================================================================================================================
Install  4 Packages
```

🌞 Démarrer le service NGINX

```
[elio@localhost ~]$ sudo systemctl start nginx
[elio@localhost ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; preset: disabled)
     Active: active (running) since Mon 2024-01-29 12:04:17 CET; 18s ago
    Process: 10751 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
    Process: 10752 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
    Process: 10753 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
   Main PID: 10754 (nginx)
      Tasks: 2 (limit: 2681)
     Memory: 2.0M
        CPU: 66ms
     CGroup: /system.slice/nginx.service
             ├─10754 "nginx: master process /usr/sbin/nginx"
             └─10755 "nginx: worker process"

Jan 29 12:04:17 localhost.localdomain systemd[1]: Starting The nginx HTTP and reverse proxy server...
Jan 29 12:04:17 localhost.localdomain nginx[10752]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
Jan 29 12:04:17 localhost.localdomain nginx[10752]: nginx: configuration file /etc/nginx/nginx.conf test is successful
Jan 29 12:04:17 localhost.localdomain systemd[1]: Started The nginx HTTP and reverse proxy server.
```

🌞 Déterminer sur quel port tourne NGINX

```
[elio@localhost ~]$ sudo ss -alnpt | grep nginx
LISTEN 0      511          0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=10755,fd=6),("nginx",pid=10754,fd=6))
LISTEN 0      511             [::]:80           [::]:*    users:(("nginx",pid=10755,fd=7),("nginx",pid=10754,fd=7))
[elio@localhost ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[elio@localhost ~]$ sudo firewall-cmd --reload
success
[elio@localhost ~]$ sudo systemctl restart firewalld
[elio@localhost ~]$ sudo firewall-cmd --list-all | grep port
  ports: 22/tcp 80/tcp
```

🌞 Déterminer les processus liés au service NGINX

```
root       10754       1  0 12:04 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx      10755   10754  0 12:04 ?        00:00:00 nginx: worker process
elio       10833     836  0 12:11 pts/0    00:00:00 grep --color=auto nginx
```

🌞 Déterminer le nom de l'utilisateur qui lance NGINX

```
[elio@localhost ~]$ cat /etc/passwd | grep root
root:x:0:0:root:/root:/bin/bash
operator:x:11:0:operator:/root:/sbin/nologin
```

🌞 Test !

```
$ curl 10.3.1.11 | head -n 7
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  7620  100  7620    0     0  2838k      0 --:--:-- --:--:-- --:--:-- 3720k
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">

```

🌞 Déterminer le path du fichier de configuration de NGINX

```
[elio@localhost ~]$ ls -al /etc/nginx/nginx.conf
-rw-r--r--. 1 root root 2334 Oct 16 20:00 /etc/nginx/nginx.conf
```

🌞 Trouver dans le fichier de conf

```
[elio@localhost ~]$ cat /etc/nginx/nginx.conf | grep server  -A 5
    server {
        listen       80;
        listen       [::]:80;
        server_name  _;
        root         /usr/share/nginx/html;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        error_page 404 /404.html;
        location = /404.html {
        }
--
# Settings for a TLS enabled server.
#
#    server {
#        listen       443 ssl http2;
#        listen       [::]:443 ssl http2;
#        server_name  _;
#        root         /usr/share/nginx/html;
#
#        ssl_certificate "/etc/pki/nginx/server.crt";
#        ssl_certificate_key "/etc/pki/nginx/private/server.key";
#        ssl_session_cache shared:SSL:1m;
#        ssl_session_timeout  10m;
#        ssl_ciphers PROFILE=SYSTEM;
#        ssl_prefer_server_ciphers on;
#
#        # Load configuration files for the default server block.
#        include /etc/nginx/default.d/*.conf;
#
#        error_page 404 /404.html;
#            location = /40x.html {
#        }
[elio@localhost ~]$ cat /etc/nginx/nginx.conf | grep include -A 5
include /usr/share/nginx/modules/*.conf;

events {
    worker_connections 1024;
}

--
    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;

    # Load modular configuration files from the /etc/nginx/conf.d directory.
    # See http://nginx.org/en/docs/ngx_core_module.html#include
    # for more information.
    include /etc/nginx/conf.d/*.conf;

    server {
        listen       80;
        listen       [::]:80;
        server_name  _;
--
        include /etc/nginx/default.d/*.conf;

        error_page 404 /404.html;
        location = /404.html {
        }

--
#        include /etc/nginx/default.d/*.conf;
#
#        error_page 404 /404.html;
#            location = /40x.html {
#        }
#
```

🌞 Créer un site web

```
[elio@localhost ~]$ cat /var/www/tp3_linux/index.html
<h1>MEOW mon premier serveur web</h1>
```

🌞 Gérer les permissions

```
[elio@localhost ~]$ sudo chown nginx:nginx /var/www/tp3_linux/index.html
[elio@localhost ~]$
```

🌞 Adapter la conf NGINX

```
[elio@localhost ~]$ sudo vim /usr/share/nginx/modules/server.conf
[elio@localhost ~]$ sudo systemctl stop nginx
[elio@localhost ~]$ sudo systemctl stop nginx
```

🌞 Visitez votre super site web

```
[elio@localhost ~]$ curl 10.3.1.11:10993
<h1>MEOW mon premier serveur web</h1>
```

🌞 Afficher le fichier de service SSH

```
[elio@localhost ~]$ systemctl status sshd
● sshd.service - OpenSSH server daemon
     Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; preset: enabled)
     Active: active (running) since Tue 2024-01-30 09:03:48 CET; 2h 56min ago
       Docs: man:sshd(8)
             man:sshd_config(5)
   Main PID: 694 (sshd)
      Tasks: 1 (limit: 2681)
     Memory: 6.4M
        CPU: 173ms
     CGroup: /system.slice/sshd.service
             └─694 "sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups"

Jan 30 09:03:48 localhost systemd[1]: Starting OpenSSH server daemon...
Jan 30 09:03:48 localhost sshd[694]: main: sshd: ssh-rsa algorithm is disabled
Jan 30 09:03:48 localhost sshd[694]: Server listening on 0.0.0.0 port 22.
Jan 30 09:03:48 localhost sshd[694]: Server listening on :: port 22.
Jan 30 09:03:48 localhost systemd[1]: Started OpenSSH server daemon.
Jan 30 09:14:30 localhost.localdomain sshd[825]: main: sshd: ssh-rsa algorithm is disabled
Jan 30 09:14:30 localhost.localdomain sshd[825]: Accepted publickey for elio from 10.3.1.1 port 64600 ssh2: RSA SHA256:hKmGAcUPwi3n4xIBsfIVp590mNluwJRqc7o9SH4homQ
Jan 30 09:14:31 localhost.localdomain sshd[825]: pam_unix(sshd:session): session opened for user elio(uid=1000) by (uid=0)
```
```
[elio@localhost ~]$ cat /usr/lib/systemd/system/sshd.service | grep ExecStart=
ExecStart=/usr/sbin/sshd -D $OPTIONS
```
```
[elio@localhost ~]$ sudo !!
sudo /usr/sbin/sshd -D $OPTIONS
[sudo] password for elio:
main: sshd: ssh-rsa algorithm is disabled
```

🌞 Afficher le fichier de service NGINX

```shell
[elio@localhost ~]$ systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; preset: disabled)
     Active: active (running) since Tue 2024-01-30 11:13:51 CET; 55min ago
    Process: 1162 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
    Process: 1163 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
    Process: 1164 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
   Main PID: 1165 (nginx)
      Tasks: 2 (limit: 2681)
     Memory: 1.9M
        CPU: 42ms
     CGroup: /system.slice/nginx.service
             ├─1165 "nginx: master process /usr/sbin/nginx"
             └─1166 "nginx: worker process"

Jan 30 11:13:51 localhost.localdomain systemd[1]: Starting The nginx HTTP and reverse proxy server...
Jan 30 11:13:51 localhost.localdomain nginx[1163]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
Jan 30 11:13:51 localhost.localdomain nginx[1163]: nginx: configuration file /etc/nginx/nginx.conf test is successful
Jan 30 11:13:51 localhost.localdomain systemd[1]: Started The nginx HTTP and reverse proxy server.
```
```shell
[elio@localhost ~]$ cat /usr/lib/systemd/system/nginx.service | grep ExecStart=
ExecStart=/usr/sbin/nginx
```
```shell
[elio@localhost ~]$ sudo /usr/sbin/nginx
nginx: [emerg] bind() to 0.0.0.0:10993 failed (98: Address already in use)
nginx: [emerg] bind() to 0.0.0.0:10993 failed (98: Address already in use)
nginx: [emerg] bind() to 0.0.0.0:10993 failed (98: Address already in use)
nginx: [emerg] bind() to 0.0.0.0:10993 failed (98: Address already in use)
nginx: [emerg] bind() to 0.0.0.0:10993 failed (98: Address already in use)
nginx: [emerg] still could not bind()
```

🌞 Créez le fichier /etc/systemd/system/tp3_nc.service

```shell
[elio@localhost ~]$ sudo vim /etc/systemd/system/tp3_nc.service
[elio@localhost ~]$ cat /etc/systemd/system/tp3_nc.service
[Unit]
Description=Super netcat tout fou

[Service]
ExecStart=/usr/bin/nc -l 10498 -k
```

🌞 Indiquer au système qu'on a modifié les fichiers de service

```
[elio@localhost ~]$ sudo systemctl daemon-reload
[elio@localhost ~]$
```

🌞 Démarrer notre service de ouf

```
[elio@localhost ~]$ sudo systemctl start tp3_nc.service
```

🌞 Vérifier que ça fonctionne
```
[elio@localhost ~]$ sudo systemctl status tp3_nc.service
● tp3_nc.service - Super netcat tout fou
     Loaded: loaded (/etc/systemd/system/tp3_nc.service; static)
     Active: active (running) since Tue 2024-01-30 12:27:01 CET; 19s ago
   Main PID: 1602 (nc)
      Tasks: 1 (limit: 2681)
     Memory: 784.0K
        CPU: 5ms
     CGroup: /system.slice/tp3_nc.service
             └─1602 /usr/bin/nc -l 10498 -k

Jan 30 12:27:01 localhost.localdomain systemd[1]: Started Super netcat tout fou.
```
🌞 Les logs de votre service

```shell
[elio@localhost ~]$ sudo journalctl -xe -u tp3_nc -f | grep start
░░ Subject: A start job for unit tp3_nc.service has finished successfully
░░ A start job for unit tp3_nc.service has finished successfully.
```
```shell
[elio@localhost ~]$ sudo journalctl -xe -u tp3_nc -f | grep curl
Feb 04 17:36:30 localhost.localdomain nc[906]: User-Agent: curl/7.76.1
```
```shell
[elio@localhost ~]$ sudo journalctl -xe -u tp3_nc -f | grep stop
░░ Subject: A stop job for unit tp3_nc.service has begun execution
░░ A stop job for unit tp3_nc.service has begun execution.
░░ Subject: A stop job for unit tp3_nc.service has finished
░░ A stop job for unit tp3_nc.service has finished.
```

🌞 S'amuser à kill le processus
```shell
[elio@localhost ~]$ pgrep nc
965
[elio@localhost ~]$ sudo !!
sudo kill 965
```

🌞 Affiner la définition du service

```shell
[elio@localhost ~]$ sudo vim /etc/systemd/system/tp3_nc.service
[elio@localhost ~]$ sudo cat /etc/systemd/system/tp3_nc.service
[Unit]
Description=Super netcat tout fou

[Service]
ExecStart=/usr/bin/nc -l 10498 -k
Restart=always

[elio@localhost ~]$ sudo systemctl daemon-reload
```

```shell
[elio@localhost ~]$ sudo systemctl start tp3_nc.service
[elio@localhost ~]$ pgrep nc
983
1032
[elio@localhost ~]$ sudo kill 983
[elio@localhost ~]$ sudo kill 1032
[elio@localhost ~]$ pgrep nc
926
983
1040
[elio@localhost ~]$ sudo systemctl status tp3_nc.service
● tp3_nc.service - Super netcat tout fou
     Loaded: loaded (/etc/systemd/system/tp3_nc.service; static)
     Active: active (running) since Sun 2024-02-04 17:58:43 CET; 30s ago
   Main PID: 1040 (nc)
      Tasks: 1 (limit: 2681)
     Memory: 792.0K
        CPU: 6ms
     CGroup: /system.slice/tp3_nc.service
             └─1040 /usr/bin/nc -l 10498 -k

Feb 04 17:58:43 localhost.localdomain systemd[1]: Started Super netcat tout fou.
```