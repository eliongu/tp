**Tp 2**
-

🌞 Trouver le chemin vers le répertoire personnel de votre utilisateur

```shell
[elio@localhost home]$ ls
elio #donc  /home/elio
```

🌞 Trouver le chemin du fichier de logs SSH

```shell
[elio@localhost log]$ sudo cat secure | grep ssh
Oct 23 11:22:48 localhost sshd[913]: Server listening on 0.0.0.0 port 22.
Oct 23 11:22:48 localhost sshd[913]: Server listening on :: port 22.
Nov 23 09:59:10 localhost sshd[693]: Server listening on 0.0.0.0 port 22.
Nov 23 09:59:10 localhost sshd[693]: Server listening on :: port 22.
Nov 23 10:03:25 localhost sshd[932]: Accepted password for elio from 10.7.1.1 port 58121 ssh2
Nov 23 10:03:25 localhost sshd[932]: pam_unix(sshd:session): session opened for user elio(uid=1000) by (uid=0)
Nov 23 10:06:29 localhost sshd[936]: Received disconnect from 10.7.1.1 port 58121:11: disconnected by user
Nov 23 10:06:29 localhost sshd[936]: Disconnected from user elio 10.7.1.1 port 58121
Nov 23 10:06:29 localhost sshd[932]: pam_unix(sshd:session): session closed for user elio
Nov 23 10:17:28 localhost sshd[967]: Accepted password for elio from 10.7.1.1 port 58213 ssh2
Nov 23 10:17:28 localhost sshd[967]: pam_unix(sshd:session): session opened for user elio(uid=1000) by (uid=0)
Nov 23 10:17:31 localhost sshd[971]: Received disconnect from 10.7.1.1 port 58213:11: disconnected by user
Nov 23 10:17:31 localhost sshd[971]: Disconnected from user elio 10.7.1.1 port 58213
Nov 23 10:17:31 localhost sshd[967]: pam_unix(sshd:session): session closed for user elio
Nov 23 10:18:27 localhost sshd[994]: Connection closed by authenticating user elio 10.7.1.1 port 58216 [preauth]
Nov 23 10:18:28 localhost sshd[996]: Connection closed by authenticating user elio 10.7.1.1 port 58217 [preauth]
Nov 23 10:19:32 localhost sshd[998]: pam_unix(sshd:auth): authentication failure; logname= uid=0 euid=0 tty=ssh ruser= rhost=10.7.1.1  user=elio
Nov 23 10:19:34 localhost sshd[998]: Failed password for elio from 10.7.1.1 port 58218 ssh2
Nov 23 10:19:37 localhost sshd[998]: Accepted password for elio from 10.7.1.1 port 58218 ssh2
Nov 23 10:19:37 localhost sshd[998]: pam_unix(sshd:session): session opened for user elio(uid=1000) by (uid=0)
Nov 23 10:19:38 localhost sshd[1003]: Received disconnect from 10.7.1.1 port 58218:11: disconnected by user
Nov 23 10:19:38 localhost sshd[1003]: Disconnected from user elio 10.7.1.1 port 58218
Nov 23 10:19:38 localhost sshd[998]: pam_unix(sshd:session): session closed for user elio
Nov 23 10:20:03 localhost sshd[1024]: Accepted publickey for elio from 10.7.1.1 port 58219 ssh2: RSA SHA256:hKmGAcUPwi3n4xIBsfIVp590mNluwJRqc7o9SH4homQ
Nov 23 10:20:03 localhost sshd[1024]: pam_unix(sshd:session): session opened for user elio(uid=1000) by (uid=0)
Dec 18 11:31:12 localhost sshd[694]: Server listening on 0.0.0.0 port 22.
Dec 18 11:31:12 localhost sshd[694]: Server listening on :: port 22.
Jan 22 10:21:07 localhost sshd[689]: Server listening on 0.0.0.0 port 22.
Jan 22 10:21:07 localhost sshd[689]: Server listening on :: port 22.
Jan 22 10:34:54 localhost sshd[687]: Server listening on 0.0.0.0 port 22.
Jan 22 10:34:54 localhost sshd[687]: Server listening on :: port 22.
Jan 22 10:41:14 localhost sshd[692]: Server listening on 0.0.0.0 port 22.
Jan 22 10:41:14 localhost sshd[692]: Server listening on :: port 22.
Jan 22 10:43:02 localhost sshd[692]: Server listening on 0.0.0.0 port 22.
Jan 22 10:43:02 localhost sshd[692]: Server listening on :: port 22.
Jan 22 10:44:23 localhost sshd[695]: Server listening on 0.0.0.0 port 22.
Jan 22 10:44:23 localhost sshd[695]: Server listening on :: port 22.
Jan 22 10:44:39 localhost sshd[854]: Accepted publickey for elio from 10.2.1.1 port 52146 ssh2: RSA SHA256:hKmGAcUPwi3n4xIBsfIVp590mNluwJRqc7o9SH4homQ
Jan 22 10:44:39 localhost sshd[854]: pam_unix(sshd:session): session opened for user elio(uid=1000) by (uid=0)
Jan 22 11:09:11 localhost sshd[857]: Received disconnect from 10.2.1.1 port 52146:11: disconnected by user
Jan 22 11:09:11 localhost sshd[857]: Disconnected from user elio 10.2.1.1 port 52146
Jan 22 11:09:11 localhost sshd[854]: pam_unix(sshd:session): session closed for user elio
Jan 22 11:09:14 localhost sshd[965]: Accepted publickey for elio from 10.2.1.1 port 52335 ssh2: RSA SHA256:hKmGAcUPwi3n4xIBsfIVp590mNluwJRqc7o9SH4homQ
Jan 22 11:09:14 localhost sshd[965]: pam_unix(sshd:session): session opened for user elio(uid=1000) by (uid=0)
```

🌞 Trouver le chemin du fichier de configuration du serveur SSH

```shell
[elio@localhost /]$ cat etc/ssh/ssh_config
#       $OpenBSD: ssh_config,v 1.35 2020/07/17 03:43:42 dtucker Exp $

# This is the ssh client system-wide configuration file.  See
# ssh_config(5) for more information.  This file provides defaults for
# users, and the values can be changed in per-user configuration files
# or on the command line.

# Configuration data is parsed as follows:
#  1. command line options
#  2. user-specific file
#  3. system-wide file
# Any configuration value is only changed the first time it is set.
# Thus, host-specific definitions should be at the beginning of the
# configuration file, and defaults at the end.

# Site-wide defaults for some commonly used options.  For a comprehensive
# list of available options, their meanings and defaults, please see the
# ssh_config(5) man page.

# Host *
#   ForwardAgent no
#   ForwardX11 no
#   PasswordAuthentication yes
#   HostbasedAuthentication no
#   GSSAPIAuthentication no
#   GSSAPIDelegateCredentials no
#   GSSAPIKeyExchange no
#   GSSAPITrustDNS no
#   BatchMode no
#   CheckHostIP yes
#   AddressFamily any
#   ConnectTimeout 0
#   StrictHostKeyChecking ask
#   IdentityFile ~/.ssh/id_rsa
#   IdentityFile ~/.ssh/id_dsa
#   IdentityFile ~/.ssh/id_ecdsa
#   IdentityFile ~/.ssh/id_ed25519
#   Port 22
#   Ciphers aes128-ctr,aes192-ctr,aes256-ctr,aes128-cbc,3des-cbc
#   MACs hmac-md5,hmac-sha1,umac-64@openssh.com
#   EscapeChar ~
#   Tunnel no
#   TunnelDevice any:any
#   PermitLocalCommand no
#   VisualHostKey no
#   ProxyCommand ssh -q -W %h:%p gateway.example.com
#   RekeyLimit 1G 1h
#   UserKnownHostsFile ~/.ssh/known_hosts.d/%k
#
# This system is following system-wide crypto policy.
# To modify the crypto properties (Ciphers, MACs, ...), create a  *.conf
#  file under  /etc/ssh/ssh_config.d/  which will be automatically
# included below. For more information, see manual page for
#  update-crypto-policies(8)  and  ssh_config(5).
Include /etc/ssh/ssh_config.d/*.conf
```

🌞 Créer un nouvel utilisateur

```shell
[elio@localhost /]$sudo useradd -m -d /home/papier_alu/marmotte -s /bin/bash marmotte
[marmotte@localhost home]$ ls
elio  marmotte  papier_alu
```

🌞 Prouver que cet utilisateur a été créé

```shell
[marmotte@localhost /]$ cat /etc/passwd | grep marmotte
marmotte:x:1001:1001::/home/papier_alu/marmotte:/bin/bash
```

🌞 Déterminer le hash du password de l'utilisateur marmotte

```shell
[elio@localhost home]$ sudo cat /etc/shadow | grep marmotte
marmotte:$6$XOHj9rbX1IUWeeGP$De0Tl90HX9uLfdknQHW4DsxW3saV4IBnc1FQ3LtOdqDuACoVneyRwoQHbkxowq6EeKkScT.1jjCwdCmfl5OGK.:19744:0:99999:7:::
```

🌞 Tapez une commande pour vous déconnecter : fermer votre session utilisateur

```shell
[elio@localhost ~]$ logout
Connection to 10.2.1.11 closed.
```

🌞 Assurez-vous que vous pouvez vous connecter en tant que l'utilisateur marmotte

```SHEll
[marmotte@localhost elio]$ ls
ls: cannot open directory '.': Permission denied
```

🌞 Lancer un processus sleep

```SHELL
[elio@localhost ~]$ ps aux | grep sleep
elio         889  0.0  0.2   5584  1020 pts/1    S+   09:53   0:00 sleep 1000
elio         891  0.0  0.4   6408  2144 pts/0    S+   09:53   0:00 grep --color=auto sleep
```

🌞 Terminez le processus sleep depuis le deuxième terminal

```SHELL
[elio@localhost ~]$ ps aux | grep sleep
elio         889  0.0  0.2   5584  1020 pts/1    S+   09:53   0:00 sleep 1000
elio         891  0.0  0.4   6408  2144 pts/0    S+   09:53   0:00 grep --color=auto sleep
[elio@localhost ~]$ kill -9 889
[elio@localhost ~]$ sleep 1000
Killed
```

🌞 Lancer un nouveau processus sleep, mais en tâche de fond

~~~shell
[elio@localhost ~]$ sleep 1000 &
[1] 894
[elio@localhost ~]$
~~~

🌞 Visualisez la commande en tâche de fond

```shell
[elio@localhost ~]$ jobs
[2]+  Running                 sleep 1000 &
```
🌞 Trouver le chemin où est stocké le programme sleep

```shell
[elio@localhost ~]$ which sleep
/usr/bin/sleep
[elio@localhost ~]$ ls -al /usr/bin/sleep | grep sleep
-rwxr-xr-x. 1 root root 36312 Apr 24  2023 /usr/bin/sleep
```

🌞 Tant qu'on est à chercher des chemins : trouver les chemins vers tous les fichiers qui s'appellent .bashrc

```shell
[elio@localhost ~]$ sudo find / -name "*.bashrc"
[sudo] password for elio:
/etc/skel/.bashrc
/root/.bashrc
/home/elio/.bashrc
/home/papier_alu/marmotte/.bashrc
```

🌞 Vérifier que

```shell
[elio@localhost ~]$ which sleep
/usr/bin/sleep
[elio@localhost ~]$ ls -al /usr/bin/sleep | grep sleep
-rwxr-xr-x. 1 root root 36312 Apr 24  2023 /usr/bin/sleep

[elio@localhost ~]$ ls -al /usr/bin/ssh | grep ssh
-rwxr-xr-x. 1 root root 851208 May  9  2023 /usr/bin/ssh
[elio@localhost ~]$ which ping
/usr/bin/ping
[elio@localhost ~]$ ls -al /usr/bin/ping | grep ping
-rwxr-xr-x. 1 root root 78352 Jan 24  2023 /usr/bin/ping
[elio@localhost ~]$
```

🌞 Installer le paquet git

```shell
[elio@localhost ~]$ sudo dnf install git wget
[sudo] password for elio:
Rocky Linux 9 - BaseOS                                                                                                      400  B/s | 4.1 kB     00:10
Rocky Linux 9 - BaseOS                                                                                                      141 kB/s | 2.2 MB     00:15
Rocky Linux 9 - AppStream                                                                                                   444  B/s | 4.5 kB     00:10
Rocky Linux 9 - AppStream                                                                                                   432 kB/s | 7.4 MB     00:17
Rocky Linux 9 - Extras                                                                                                      288  B/s | 2.9 kB     00:10
Rocky Linux 9 - Extras                                                                                                      906  B/s |  14 kB     00:15
Dependencies resolved.
============================================================================================================================================================
 Package                                      Architecture                 Version                                    Repository                       Size
============================================================================================================================================================
Installing:
 git
```

🌞 Utiliser une commande pour lancer git

```shell
[elio@localhost ~]$ which git
/usr/bin/git
[elio@localhost ~]$ ls -al /usr/bin/git | grep git
-rwxr-xr-x. 1 root root 3960424 May 22  2023 /usr/bin/git
```

🌞 Installer le paquet nginx

```shell
[elio@localhost ~]$ sudo dnf install nginx
[sudo] password for elio:
Last metadata expiration check: 0:10:06 ago on Tue 23 Jan 2024 10:39:12 AM CET.
Dependencies resolved.
============================================================================================================================================================
 Package                                  Architecture                  Version                                      Repository                        Size
============================================================================================================================================================
Installing:
 nginx
```

🌞 Déterminer

```shell
[elio@localhost ~]$ ls /var/log/
anaconda  btmp    cron             dnf.log      firewalld   kdump.log  maillog   nginx    README  spooler  tallylog
audit     chrony  dnf.librepo.log  dnf.rpm.log  hawkey.log  lastlog    messages  private  secure  sssd     wtmp

[elio@localhost ~]$ ls /etc/nginx/
conf.d        fastcgi.conf.default    koi-utf     mime.types.default  scgi_params          uwsgi_params.default
default.d     fastcgi_params          koi-win     nginx.conf          scgi_params.default  win-utf
fastcgi.conf  fastcgi_params.default  mime.types  nginx.conf.default  uwsgi_params
```

🌞 Mais aussi déterminer...

```shell
[elio@localhost ~]$ sudo cat /var/log/dnf.log | grep http
2024-01-23T10:49:18+0100 DEBUG ---> Package rocky-logos-httpd.noarch 90.14-2.el9 will be installed
 rocky-logos-httpd                        noarch                        90.14-2.el9                                  appstream                         24 k
2024-01-23T10:49:49+0100 DEBUG Installed: rocky-logos-httpd-90.14-2.el9.noarch
2024-01-23T10:49:49+0100 DDEBUG /var/cache/dnf/appstream-25485261a76941d3/packages/rocky-logos-httpd-90.14-2.el9.noarch.rpm removed
```

🌞 Récupérer le fichier meow

```shell
[elio@localhost ~]$ sudo wget https://gitlab.com/it4lik/b1-linux-2023/-/raw/master/tp/2/meow
[sudo] password for elio:
--2024-01-23 11:11:28--  https://gitlab.com/it4lik/b1-linux-2023/-/raw/master/tp/2/meow
Resolving gitlab.com (gitlab.com)... 172.65.251.78, 2606:4700:90:0:f22e:fbec:5bed:a9b9
Connecting to gitlab.com (gitlab.com)|172.65.251.78|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 18016947 (17M) [application/octet-stream]
Saving to: ‘meow’

meow                                   100%[============================================================================>]  17.18M  3.13MB/s    in 5.7s

2024-01-23 11:11:39 (3.02 MB/s) - ‘meow’ saved [18016947/18016947]
```

🌞 Trouver le dossier dawa/

```shell
[elio@localhost ~]$ file /home/elio/meow
/home/elio/meow: Zip archive data, at least v2.0 to extract
[elio@localhost ~]$ mv meow meow.zip
[elio@localhost ~]$ ls
meow.zip

[elio@localhost ~]$ unzip meow.zip
Archive:  meow.zip
  inflating: meow
[elio@localhost ~]$ ls
meow  meow.zip

[elio@localhost ~]$ mv meow meow.xz
[elio@localhost ~]$ xz -d meow.xz
[elio@localhost ~]$ ls
meow
[elio@localhost ~]$ file /home/elio/meow
/home/elio/meow: bzip2 compressed data, block size = 900k
[elio@localhost ~]$ mv meow.bzip2 meow.bz2
[elio@localhost ~]$ bzip2 -d meow.bz2

Extracting from /home/elio/meow.rar

Extracting  meow                                                      OK
All OK
[elio@localhost ~]$ ls
meow  meow.rar
[elio@localhost ~]$ mv meow meow.gz
[elio@localhost ~]$ gzip -d meow.gz
[elio@localhost ~]$ ls
meow  meow.rar  meow.zip
[elio@localhost ~]$ file  meow
meow: POSIX tar archive (GNU)

  235  file  meow
  236  sudo dnf install tar
  237  mv meow meow.tar
  238  tar -xvf meow.tar

[elio@localhost ~]$ ls
dawa  meow.rar  meow.tar  meow.zip
```

🌞 Dans le dossier dawa/, déterminer le chemin vers

```shell
[elio@localhost ~]$ find /home/elio/dawa -type f -size 15M -print 2>/dev/null
/home/elio/dawa/folder31/19/file39

[elio@localhost ~]$ grep -rlE '^[7]+$' /home/elio/dawa | xargs grep -lE '7.*7'
/home/elio/dawa/folder31/19/file39 ##celui ci ne contient pas que des 7 mais peu importe la commande il reste toujours dans ma recherche
/home/elio/dawa/folder43/38/file41


[elio@localhost ~]$ find /home/elio/dawa -type f -name 'cookie' -exec cat {} \;
found me, gg
[elio@localhost ~]$ find /home/elio/dawa -type f -name 'cookie' -print
/home/elio/dawa/folder14/25/cookie

[elio@localhost ~]$ find /home/elio/dawa -type f -name '.*' -print
/home/elio/dawa/folder32/14/.hidden_file
/home/elio/dawa/folder43/38/.file41.swp

[elio@localhost ~]$ find /home/elio/dawa -type f -newermt 2014-01-01 ! -newermt 2014-12-31 -print
/home/elio/dawa/folder36/40/file43

[elio@localhost ~]$ find /home/elio/dawa -type f -path "*/folder*/*/*/*/*/*"
/home/elio/dawa/folder37/45/23/43/54/file43
```