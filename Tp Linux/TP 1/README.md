**TP 1**
-

🌞 Supprimer des fichiers

```
[elio@localhost boot]$ sudo rm -r *
[elio@localhost ~]$ exit
(Après ça marche plus)
```

🌞 Mots de passe

```
[elio@localhost ~]$ sudo rm /etc/passwd


[elio@localhost /]$ sudo rm -r usr/
```

🌞 Effacer le contenu du disque dur

```
[elio@localhost ~]$ sudo dd if=/dev/zero of=/dev/sda bs=4M status=progress

```


🌞 Reboot automatique

```
[elio@localhost ~]$ sudo vim ~/.bashrc
# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
if [ -d ~/.bashrc.d ]; then
        for rc in ~/.bashrc.d/*; do
                if [ -f "$rc" ]; then
                        . "$rc"
                fi
        done
fi

unset rc

sudo reboot

```





🌞 Trouvez 4 autres façons de détuire la machine

```
[elio@localhost ~]$ sudo chmod 777 -R /

[elio@localhost ~]$ sudo dd if=/dev/random of=/dev/sda bs=4M status=progress

```