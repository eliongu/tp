# TP 6

🌞 Installer le serveur Apache

```shell
ServerRoot "/etc/httpd"

Listen 80

Include conf.modules.d/*.conf

User apache
Group apache


ServerAdmin root@localhost


<Directory />
    AllowOverride none
    Require all denied
</Directory>


DocumentRoot "/var/www/html"

<Directory "/var/www">
    AllowOverride None
    Require all granted
</Directory>

<Directory "/var/www/html">
    Options Indexes FollowSymLinks
```

🌞 Démarrer le service Apache

```shell
[elio@localhost ~]$ sudo systemctl start httpd
[sudo] password for elio:
[elio@localhost ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
[elio@localhost ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; preset: disabled)
     Active: active (running) since Tue 2024-03-05 11:16:12 CET; 1min 5s ago
       Docs: man:httpd.service(8)
   Main PID: 31277 (httpd)
     Status: "Total requests: 0; Idle/Busy workers 100/0;Requests/sec: 0; Bytes served/sec:   0 B/sec"
      Tasks: 213 (limit: 2681)
     Memory: 15.7M
        CPU: 172ms
     CGroup: /system.slice/httpd.service
             ├─31277 /usr/sbin/httpd -DFOREGROUND
             ├─31278 /usr/sbin/httpd -DFOREGROUND
             ├─31279 /usr/sbin/httpd -DFOREGROUND
             ├─31280 /usr/sbin/httpd -DFOREGROUND
             └─31281 /usr/sbin/httpd -DFOREGROUND

Mar 05 11:16:12 web.tp6.linux systemd[1]: Starting The Apache HTTP Server...
Mar 05 11:16:12 web.tp6.linux systemd[1]: Started The Apache HTTP Server.
Mar 05 11:16:12 web.tp6.linux httpd[31277]: Server configured, listening on: port 80
[elio@localhost ~]$ sudo ss -alnpt
State           Recv-Q          Send-Q                   Local Address:Port                   Peer Address:Port
Process
LISTEN          0               128                            0.0.0.0:22                          0.0.0.0:*
 users:(("sshd",pid=31231,fd=3))
LISTEN          0               511                                  *:80                                *:*
 users:(("httpd",pid=31281,fd=4),("httpd",pid=31280,fd=4),("httpd",pid=31279,fd=4),("httpd",pid=31277,fd=4))
LISTEN          0               128                               [::]:22                             [::]:*
 users:(("sshd",pid=31231,fd=4))
[elio@localhost ~]$ sudo journalctl -xe -u httpd
~
Mar 05 11:16:12 web.tp6.linux systemd[1]: Starting The Apache HTTP Server...
░░ Subject: A start job for unit httpd.service has begun execution
░░ Defined-By: systemd
░░ Support: https://wiki.rockylinux.org/rocky/support
░░
░░ A start job for unit httpd.service has begun execution.
░░
░░ The job identifier is 2073.
Mar 05 11:16:12 web.tp6.linux systemd[1]: Started The Apache HTTP Server.
░░ Subject: A start job for unit httpd.service has finished successfully
░░ Defined-By: systemd
░░ Support: https://wiki.rockylinux.org/rocky/support
░░
░░ A start job for unit httpd.service has finished successfully.
░░
░░ The job identifier is 2073.
Mar 05 11:16:12 web.tp6.linux httpd[31277]: Server configured, listening on: port 80
[elio@localhost ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[elio@localhost ~]$ sudo firewall-cmd --reload
success
[elio@localhost ~]$ ss -alnpt
State         Recv-Q        Send-Q               Local Address:Port               Peer Address:Port       Process
LISTEN        0             128                        0.0.0.0:22                      0.0.0.0:*
LISTEN        0             511                              *:80                            *:*
LISTEN        0             128                           [::]:22                         [::]:*
```

🌞 TEST

```shell
[elio@localhost ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; preset: disabled)
     Active: active (running) since Tue 2024-03-05 11:16:12 CET; 1min 5s ago
       Docs: man:httpd.service(8)
   Main PID: 31277 (httpd)
     Status: "Total requests: 0; Idle/Busy workers 100/0;Requests/sec: 0; Bytes served/sec:   0 B/sec"
      Tasks: 213 (limit: 2681)
     Memory: 15.7M
        CPU: 172ms
     CGroup: /system.slice/httpd.service
             ├─31277 /usr/sbin/httpd -DFOREGROUND
             ├─31278 /usr/sbin/httpd -DFOREGROUND
             ├─31279 /usr/sbin/httpd -DFOREGROUND
             ├─31280 /usr/sbin/httpd -DFOREGROUND
             └─31281 /usr/sbin/httpd -DFOREGROUND
[elio@localhost ~]$ sudo systemctl is-enabled httpd
[sudo] password for elio:
enabled
```

```shell
[elio@localhost ~]$ curl localhost
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/

      html {
        height: 100%;
        width: 100%;
      }
        body {
  background: rgb(20,72,50);
  background: -moz-linear-gradient(180deg, rgba(23,43,70,1) 30%, rgba(0,0,0,1) 90%)  ;
  background: -webkit-linear-gradient(180deg, rgba(23,43,70,1) 30%, rgba(0,0,0,1) 90%) ;
  background: linear-gradient(180deg, rgba(23,43,70,1) 30%, rgba(0,0,0,1) 90%);
  background-repeat: no-repeat;
  background-attachment: fixed;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#3c6eb4",endColorstr="#3c95b4",GradientType=1);
        color: white;
        font-size: 0.9em;
        font-weight: 400;
        font-family: 'Montserrat', sans-serif;
        margin: 0;
        padding: 10em 6em 10em 6em;
        box-sizing: border-box;

      }


  h1 {
    text-align: center;
    margin: 0;
    padding: 0.6em 2em 0.4em;
    color: #fff;
    font-weight: bold;
    font-family: 'Montserrat', sans-serif;
    font-size: 2em;
  }
  h1 strong {
    font-weight: bolder;
    font-family: 'Montserrat', sans-serif;
  }
  h2 {
    font-size: 1.5em;
    font-weight:bold;
  }

  .title {
    border: 1px solid black;
    font-weight: bold;
    position: relative;
    float: right;
    width: 150px;
    text-align: center;
    padding: 10px 0 10px 0;
    margin-top: 0;
  }

  .description {
    padding: 45px 10px 5px 10px;
    clear: right;
    padding: 15px;
  }

  .section {
    padding-left: 3%;
   margin-bottom: 10px;
  }

  img {

    padding: 2px;
    margin: 2px;
  }
  a:hover img {
    padding: 2px;
    margin: 2px;
  }

  :link {
    color: rgb(199, 252, 77);
    text-shadow:
  }
  :visited {
    color: rgb(122, 206, 255);
  }
  a:hover {
    color: rgb(16, 44, 122);
  }
  .row {
    width: 100%;
    padding: 0 10px 0 10px;
  }

  footer {
    padding-top: 6em;
    margin-bottom: 6em;
    text-align: center;
    font-size: xx-small;
    overflow:hidden;
    clear: both;
  }

  .summary {
    font-size: 140%;
    text-align: center;
  }

  #rocky-poweredby img {
    margin-left: -10px;
  }

  #logos img {
    vertical-align: top;
  }

  /* Desktop  View Options */

  @media (min-width: 768px)  {

    body {
      padding: 10em 20% !important;
    }

    .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6,
    .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12 {
      float: left;
    }

    .col-md-1 {
      width: 8.33%;
    }
    .col-md-2 {
      width: 16.66%;
    }
    .col-md-3 {
      width: 25%;
    }
    .col-md-4 {
      width: 33%;
    }
    .col-md-5 {
      width: 41.66%;
    }
    .col-md-6 {
      border-left:3px ;
      width: 50%;


    }
    .col-md-7 {
      width: 58.33%;
    }
    .col-md-8 {
      width: 66.66%;
    }
    .col-md-9 {
      width: 74.99%;
    }
    .col-md-10 {
      width: 83.33%;
    }
    .col-md-11 {
      width: 91.66%;
    }
    .col-md-12 {
      width: 100%;
    }
  }

  /* Mobile View Options */
  @media (max-width: 767px) {
    .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6,
    .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
      float: left;
    }

    .col-sm-1 {
      width: 8.33%;
    }
    .col-sm-2 {
      width: 16.66%;
    }
    .col-sm-3 {
      width: 25%;
    }
    .col-sm-4 {
      width: 33%;
    }
    .col-sm-5 {
      width: 41.66%;
    }
    .col-sm-6 {
      width: 50%;
    }
    .col-sm-7 {
      width: 58.33%;
    }
    .col-sm-8 {
      width: 66.66%;
    }
    .col-sm-9 {
      width: 74.99%;
    }
    .col-sm-10 {
      width: 83.33%;
    }
    .col-sm-11 {
      width: 91.66%;
    }
    .col-sm-12 {
      width: 100%;
    }
    h1 {
      padding: 0 !important;
    }
  }


  </style>
  </head>
  <body>
    <h1>HTTP Server <strong>Test Page</strong></h1>

    <div class='row'>

      <div class='col-sm-12 col-md-6 col-md-6 '></div>
          <p class="summary">This page is used to test the proper operation of
            an HTTP server after it has been installed on a Rocky Linux system.
            If you can read this page, it means that the software is working
            correctly.</p>
      </div>

      <div class='col-sm-12 col-md-6 col-md-6 col-md-offset-12'>


        <div class='section'>
          <h2>Just visiting?</h2>

          <p>This website you are visiting is either experiencing problems or
          could be going through maintenance.</p>

          <p>If you would like the let the administrators of this website know
          that you've seen this page instead of the page you've expected, you
          should send them an email. In general, mail sent to the name
          "webmaster" and directed to the website's domain should reach the
          appropriate person.</p>

          <p>The most common email address to send to is:
          <strong>"webmaster@example.com"</strong></p>

          <h2>Note:</h2>
          <p>The Rocky Linux distribution is a stable and reproduceable platform
          based on the sources of Red Hat Enterprise Linux (RHEL). With this in
          mind, please understand that:

        <ul>
          <li>Neither the <strong>Rocky Linux Project</strong> nor the
          <strong>Rocky Enterprise Software Foundation</strong> have anything to
          do with this website or its content.</li>
          <li>The Rocky Linux Project nor the <strong>RESF</strong> have
          "hacked" this webserver: This test page is included with the
          distribution.</li>
        </ul>
        <p>For more information about Rocky Linux, please visit the
          <a href="https://rockylinux.org/"><strong>Rocky Linux
          website</strong></a>.
        </p>
        </div>
      </div>
      <div class='col-sm-12 col-md-6 col-md-6 col-md-offset-12'>
        <div class='section'>

          <h2>I am the admin, what do I do?</h2>

        <p>You may now add content to the webroot directory for your
        software.</p>

        <p><strong>For systems using the
        <a href="https://httpd.apache.org/">Apache Webserver</strong></a>:
        You can add content to the directory <code>/var/www/html/</code>.
        Until you do so, people visiting your website will see this page. If
        you would like this page to not be shown, follow the instructions in:
        <code>/etc/httpd/conf.d/welcome.conf</code>.</p>

        <p><strong>For systems using
        <a href="https://nginx.org">Nginx</strong></a>:
        You can add your content in a location of your
        choice and edit the <code>root</code> configuration directive
        in <code>/etc/nginx/nginx.conf</code>.</p>

        <div id="logos">
          <a href="https://rockylinux.org/" id="rocky-poweredby"><img src="icons/poweredby.png" alt="[ Powered by Rocky Linux ]" /></a> <!-- Rocky -->
          <img src="poweredby.png" /> <!-- webserver -->
        </div>
      </div>
      </div>

      <footer class="col-sm-12">
      <a href="https://apache.org">Apache&trade;</a> is a registered trademark of <a href="https://apache.org">the Apache Software Foundation</a> in the United States and/or other countries.<br />
      <a href="https://nginx.org">NGINX&trade;</a> is a registered trademark of <a href="https://">F5 Networks, Inc.</a>.
      </footer>

  </body>
</html>
```


```shell
$ curl 10.6.1.11
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/

      html {
        height: 100%;
        width: 100%;
        [...]
        <a href="https://nginx.org">Nginx</strong></a>:
        You can add your content in a location of your
        choice and edit the <code>root</code> configuration directive
        in <code>/etc/nginx/nginx.conf</code>.</p>

        <div id="logos">
          <a href="https://rockylinux.org/" id="rocky-poweredby"><img src="icons/poweredby.png" alt="[ Powered by Rocky Linux ]" /></a> <!-- Rocky -->
          <img src="poweredby.png" /> <!-- webserver -->
        </div>
      </div>
      </div>

      <footer class="col-sm-12">
      <a href="https://apache.org">Apache&trade;</a> is a registered trademark of <a href="https://apache.org">the Apache Software Foundation</a> in the United States and/or other countries.<br />
      <a href="https://nginx.org">NGINX&trade;</a> is a registered trademark of <a href="https://">F5 Networks, Inc.</a>.
      </footer>

  </body>
</html>
```

🌞 Le service Apache...

```shell
[elio@localhost ~]$ sudo cat /usr/lib/systemd/system/httpd.service
# See httpd.service(8) for more information on using the httpd service.

# Modifying this file in-place is not recommended, because changes
# will be overwritten during package upgrades.  To customize the
# behaviour, run "systemctl edit httpd" to create an override unit.

# For example, to pass additional options (such as -D definitions) to
# the httpd binary at startup, create an override unit (as is done by
# systemctl edit) and enter the following:

#       [Service]
#       Environment=OPTIONS=-DMY_DEFINE

[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true
OOMPolicy=continue

[Install]
WantedBy=multi-user.target
```


🌞 Déterminer sous quel utilisateur tourne le processus Apache

```shell
[elio@localhost ~]$ sudo cat /etc/httpd/conf/httpd.conf | grep User
User apache
    LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
      LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" %I %O" combinedio
```
```shell
[elio@localhost ~]$ ps -ef | grep httpd
root       31277       1  0 11:16 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     31278   31277  0 11:16 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     31279   31277  0 11:16 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     31280   31277  0 11:16 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     31281   31277  0 11:16 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
elio       31667     841  0 12:14 pts/0    00:00:00 grep --color=auto httpd
```
```
[elio@localhost ~]$ ls -al /usr/share/testpage/
total 12
drwxr-xr-x.  2 root root   24 Mar  5 11:06 .
drwxr-xr-x. 83 root root 4096 Mar  5 11:06 ..
-rw-r--r--.  1 root root 7620 Feb 21 14:12 index.html
```

🌞 Changer l'utilisateur utilisé par Apache
```shell
[elio@web ~]$ sudo useradd -u 5000 -g 48 -d /usr/share/httpd -s /sbin/nologin server
[elio@web ~]$ sudo vim /etc/httpd/conf/httpd.conf
[elio@web ~]$ sudo cat /etc/httpd/conf/httpd.conf | grep User
User server
    LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
      LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" %I %O" combinedio
[elio@web ~]$ sudo systemctl restart apache
Failed to restart apache.service: Unit apache.service not found.
[elio@web ~]$ sudo systemctl restart httpd
[elio@web ~]$ ps aux | grep httpd
root        1667  0.1  2.5  20404 11596 ?        Ss   19:05   0:00 /usr/sbin/httpd -DFOREGROUND
server      1668  0.0  1.5  21684  7272 ?        S    19:05   0:00 /usr/sbin/httpd -DFOREGROUND
server      1669  0.0  1.9 1210624 8916 ?        Sl   19:05   0:00 /usr/sbin/httpd -DFOREGROUND
server      1670  0.0  1.9 1079488 8916 ?        Sl   19:05   0:00 /usr/sbin/httpd -DFOREGROUND
server      1671  0.0  1.9 1079488 8916 ?        Sl   19:05   0:00 /usr/sbin/httpd -DFOREGROUND
elio        1884  0.0  0.4   6408  2172 pts/0    S+   19:05   0:00 grep --color=auto httpd
```


🌞 Faites en sorte que Apache tourne sur un autre port
```shell
[elio@web ~]$ sudo cat /etc/httpd/conf/httpd.conf | grep List
Listen 8080
[elio@web ~]$ sudo firewall-cmd --zone=public --add-port=8080/tcp --permanent
success
[elio@web ~]$ sudo firewall-cmd --zone=public --remove-port=80/tcp --permanent
success
[elio@web ~]$ sudo firewall-cmd --reload
success
[elio@web ~]$ sudo systemctl restart httpd
[elio@web ~]$ ss -tln | grep 8080
LISTEN 0      511                *:8080            *:*
[elio@web ~]$ curl http://localhost:8080
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/

      html {
        height: 100%;
        width: 100%;
      }
        body { 
[...]
 </body>
</html>
```

📁 Fichier /etc/httpd/conf/httpd.conf

```

ServerRoot "/etc/httpd"

Listen 8080

Include conf.modules.d/*.conf

User server
Group apache


ServerAdmin root@localhost


<Directory />
    AllowOverride none
    Require all denied
</Directory>


DocumentRoot "/var/www/html"

<Directory "/var/www">
    AllowOverride None
    Require all granted
</Directory>

<Directory "/var/www/html">
    Options Indexes FollowSymLinks

    AllowOverride None

    Require all granted
</Directory>

<IfModule dir_module>
    DirectoryIndex index.html
</IfModule>

<Files ".ht*">
    Require all denied
</Files>

ErrorLog "logs/error_log"

LogLevel warn

<IfModule log_config_module>
    LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
    LogFormat "%h %l %u %t \"%r\" %>s %b" common

    <IfModule logio_module>
      LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" %I %O" combinedio
    </IfModule>


    CustomLog "logs/access_log" combined
</IfModule>

<IfModule alias_module>


    ScriptAlias /cgi-bin/ "/var/www/cgi-bin/"

</IfModule>

<Directory "/var/www/cgi-bin">
    AllowOverride None
    Options None
    Require all granted
</Directory>

<IfModule mime_module>
    TypesConfig /etc/mime.types

    AddType application/x-compress .Z
    AddType application/x-gzip .gz .tgz



    AddType text/html .shtml
    AddOutputFilter INCLUDES .shtml
</IfModule>

AddDefaultCharset UTF-8

<IfModule mime_magic_module>
    MIMEMagicFile conf/magic
</IfModule>


EnableSendfile on

IncludeOptional conf.d/*.conf
```

🌞 Install de MariaDB sur db.tp6.linux

```shell
[elio@db ~]$ yes | sudo yum install mariadb-server
Last metadata expiration check: 0:25:17 ago on Sun 24 Mar 2024 06:58:56 PM CET.
Dependencies resolved.
============================================================================
 Package                      Arch   Version                Repo       Size
============================================================================
Installing:
 mariadb-server               x86_64 3:10.5.22-1.el9_2      appstream 9.6 M
Upgrading:
 audit                        x86_64 3.0.7-104.el9          baseos    251 k
 audit-libs                   x86_64 3.0.7-104.el9          baseos    116 k
 libsemanage                  x86_64 3.5-2.el9              baseos    117 k
 policycoreutils              x86_64 3.5-3.el9_3            baseos    206 k
Installing dependencies:
 checkpolicy                  x86_64 3.5-1.el9              appstream 345 k
 mariadb                      x86_64 3:10.5.22-1.el9_2      appstream 1.6 M
 mariadb-common               x86_64 3:10.5.22-1.el9_2      appstream  27 k
 mariadb-connector-c
[...]
python3-setuptools-53.0.0-12.el9.noarch

Complete!
```
```shell
[elio@db ~]$ sudo systemctl start mariadb
[sudo] password for elio:
Sorry, try again.
[sudo] password for elio:
[elio@db ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
```
```shell
riadb.service - MariaDB 10.5 database server
     Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; pres>
     Active: active (running) since Sun 2024-03-24 19:29:38 CET; 1min 44s a>
       Docs: man:mariadbd(8)
             https://mariadb.com/kb/en/library/systemd/
   Main PID: 12880 (mariadbd)
     Status: "Taking your SQL requests now..."
      Tasks: 8 (limit: 2681)
     Memory: 68.6M
        CPU: 450ms
     CGroup: /system.slice/mariadb.service
             └─12880 /usr/libexec/mariadbd --basedir=/usr

Mar 24 19:29:38 db.tp.linux mariadb-prepare-db-dir[12838]: The second is my>
Mar 24 19:29:38 db.tp.linux mariadb-prepare-db-dir[12838]: you need to be t>
Mar 24 19:29:38 db.tp.linux mariadb-prepare-db-dir[12838]: After connecting>
Mar 24 19:29:38 db.tp.linux mariadb-prepare-db-dir[12838]: able to connect >
Mar 24 19:29:38 db.tp.linux mariadb-prepare-db-dir[12838]: See the MariaDB >
Mar 24 19:29:38 db.tp.linux mariadb-prepare-db-dir[12838]: Please report an>
Mar 24 19:29:38 db.tp.linux mariadb-prepare-db-dir[12838]: The latest infor>
Mar 24 19:29:38 db.tp.linux mariadb-prepare-db-dir[12838]: Consider joining>
Mar 24 19:29:38 db.tp.linux mariadb-prepare-db-dir[12838]: https://mariadb.>
Mar 24 19:29:38 db.tp.linux systemd[1]: Started MariaDB
```


🌞 Port utilisé par MariaDB

```shell
[elio@db ~]$ sudo ss -tln
State   Recv-Q  Send-Q   Local Address:Port   Peer Address:Port Process
LISTEN  0       128            0.0.0.0:22          0.0.0.0:*
LISTEN  0       128               [::]:22             [::]:*
## LISTEN  0       80                   *:3306              *:* ###
```
```shell
[elio@db ~]$ sudo firewall-cmd --zone=public --add-port=3306/tcp --permanent
success
[elio@db ~]$ sudo firewall-cmd --reload
success
[elio@db ~]$ sudo mysql_secure_installation

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user. If you've just installed MariaDB, and
haven't set the root password yet, you should just press enter here.

Enter current password for root (enter for none):
[...]
Thanks for using MariaDB!
```

🌞 Processus liés à MariaDB

```shell
[elio@db ~]$ ps aux | grep mariadb
mysql      12880  0.0 17.1 1085356 80724 ?       Ssl  19:29   0:00 /usr/libexec/mariadbd --basedir=/usr
elio       13069  0.0  0.4   6408  2148 pts/0    S+   19:44   0:00 grep --color=auto mariadb

```


🌞 Préparation de la base pour NextCloud
```shell
[elio@db ~]$ sudo mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 13
Server version: 10.5.22-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]>
MariaDB [(none)]>
MariaDB [(none)]> none
    -> Ctrl-C -- exit!
Aborted
[elio@db ~]$ sudo mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 14
Server version: 10.5.22-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> CREATE USER 'nextcloud'@'10.6.1.11' IDENTIFIED BY 'pewpewpew';
Query OK, 0 rows affected (0.003 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.6.1.11';
FLUSH PRIVILEGES;Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)
```

🌞 Exploration de la base de données

```shell
[elio@web ~]$ dnf provides mysql
Rocky Linux 9 - BaseOS                   1.1 MB/s | 2.2 MB     00:02
Rocky Linux 9 - AppStream                1.4 MB/s | 7.4 MB     00:05
Rocky Linux 9 - Extras                    33 kB/s |  14 kB     00:00
mysql-8.0.36-1.el9_3.x86_64 : MySQL client programs and shared libraries
Repo        : appstream
Matched from:
Provide    : mysql = 8.0.36-1.el9_3
```
```shell
mysql -u nextcloud -h 10.6.1.12 -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 16
Server version: 5.5.5-10.5.22-MariaDB MariaDB Server

Copyright (c) 2000, 2024, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> SHOW TABLES;
ERROR 1046 (3D000): No database selected
mysql> SHOW TABLES;
ELECT * FROM ERROR 1046 (3D000): No database selected
mysql> SELECT * FROM server;
ERROR 1046 (3D000): No database selected
mysql> SHOW DATABASES;
E <DATABASE_+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.00 sec
```
🌞 Trouver une commande SQL qui permet de lister tous les utilisateurs de la base de données

🌞 Install de PHP
```shell
[elio@db ~]$ sudo vim /etc/httpd/conf/httpd.conf
[elio@db ~]$ sudo systemctl restart httpd
[elio@db ~]$ sudo dnf install php
```

🌞 Récupérer NextCloud

```
[elio@db ~]$ ls -al /var/www/tp6_nextcloud/
total 4
drwxr-xr-x.  3 root root   23 Mar 24 20:24 .
drwxr-xr-x.  5 root root   54 Mar 24 20:16 ..
drwxr-xr-x. 13 root root 4096 Feb 29 08:49 nextcloud
[elio@db ~]$ sudo chown -R apache:apache /var/www/tp6_nextcloud/
```
🌞 Adapter la configuration d'Apache

```shell
[elio@db ~]$ sudo nano /etc/httpd/conf.d/nextcloud.conf
[elio@db ~]$ sudo systemctl restart httpd
```

🌞 Installez les deux modules PHP dont NextCloud vous parle
```shell
[elio@web ~]$ sudo dnf install php-gd
[sudo] password for elio:
Last metadata expiration check: 1:35:15 ago on Sun 24 Mar 2024 06:55:59 PM CET.
Dependencies resolved.
=========================================================================
 Package            Arch        Version             Repository      Size
=========================================================================
Installing:
 php-gd             x86_64      8.0.30-1.el9_2      appstream       39 k
Installing dependencies:
 fontconfig         x86_64      2.14.0-2.el9_1      appstream      274 k
 gd                 x86_64      2.3.2-3.el9         appstream      131 k
 jbigkit-libs       x86_64      2.1-23.el9          appstream       52 k
 libX11             x86_64      1.7.0-8.el9         appstream      650 k
 libX11-common      noarch      1.7.0-8.el9         appstream      151 k
 libXau             x86_64      1.0.9-8.el9         appstream       30 k
 libXpm             x86_64      3.5.13-8.el9_1      appstream       57 k
 libjpeg-turbo      x86_64      2.0.90-6.el9_1      appstream      175 k
 libtiff            x86_64      4.4.0-10.el9        appstream      196 k
 libwebp            x86_64      1.2.0-8.el9         appstream      276 k
 libxcb             x86_64      1.13.1-9.el9        appstream      224 k
 php-common         x86_64      8.0.30-1.el9_2      appstream      665 k
 xml-common         noarch      0.6.3-58.el9        appstream       31 k

Transaction Summary
[...]
Complete!
[elio@web ~]$ yes | sudo dnf install php-curl
Last metadata expiration check: 1:35:31 ago on Sun 24 Mar 2024 06:55:59 PM CET.
Package php-common-8.0.30-1.el9_2.x86_64 is already installed.
Dependencies resolved.
Nothing to do.
Complete!
```

🌞 Pour que NextCloud utilise la base de données, ajoutez aussi

```
[elio@web ~]$ yes | sudo dnf install php-pdo
Last metadata expiration check: 1:37:58 ago on Sun 24 Mar 2024 06:55:59 PM CET.
Dependencies resolved.
=========================================================================
 Package       Architecture Version                Repository       Size
=========================================================================
Installing:
 php-pdo       x86_64       8.0.30-1.el9_2         appstream        81 k

Transaction Summary
=========================================================================
Install  1 Package

Total download size: 81 k
Installed size: 229 k
Is this ok [y/N]: Downloading Packages:
php-pdo-8.0.30-1.el9_2.x86_64.rpm        193 kB/s |  81 kB     00:00
-------------------------------------------------------------------------
Total                                     64 kB/s |  81 kB     00:01
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                 1/1
  Installing       : php-pdo-8.0.30-1.el9_2.x86_64                   1/1
  Running scriptlet: php-pdo-8.0.30-1.el9_2.x86_64                   1/1
  Verifying        : php-pdo-8.0.30-1.el9_2.x86_64                   1/1

Installed:
  php-pdo-8.0.30-1.el9_2.x86_64

Complete!
[elio@web ~]$ yes | sudo dnf install php-mysqlnd
Last metadata expiration check: 1:38:07 ago on Sun 24 Mar 2024 06:55:59 PM CET.
Dependencies resolved.
=========================================================================
 Package          Arch        Version               Repository      Size
=========================================================================
Installing:
 php-mysqlnd      x86_64      8.0.30-1.el9_2        appstream      148 k

Transaction Summary
=========================================================================
Install  1 Package

Total download size: 148 k
Installed size: 450 k
Is this ok [y/N]: Downloading Packages:
php-mysqlnd-8.0.30-1.el9_2.x86_64.rpm    416 kB/s | 148 kB     00:00
-------------------------------------------------------------------------
Total                                    229 kB/s | 148 kB     00:00
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                 1/1
  Installing       : php-mysqlnd-8.0.30-1.el9_2.x86_64               1/1
  Running scriptlet: php-mysqlnd-8.0.30-1.el9_2.x86_64               1/1
  Verifying        : php-mysqlnd-8.0.30-1.el9_2.x86_64               1/1

Installed:
  php-mysqlnd-8.0.30-1.el9_2.x86_64

Complete!
```