**TP 7**
-

🌞 Effectuez une connexion SSH en vérifiant le fingerprint
```
 ssh elio@10.7.1.11
The authenticity of host '10.7.1.11 (10.7.1.11)' can't be established.
ED25519 key fingerprint is SHA256:BpvSJEF2Xvw7TxoLxlzazSVYgSGV/O7U1Ufrr/xa45c.
This host key is known by the following other names/addresses:
    C:\Users\elion/.ssh/known_hosts:4: 10.3.1.11
    C:\Users\elion/.ssh/known_hosts:7: 10.3.1.12
    C:\Users\elion/.ssh/known_hosts:8: 10.3.1.13
    C:\Users\elion/.ssh/known_hosts:9: 10.3.1.14
    C:\Users\elion/.ssh/known_hosts:10: 10.3.1.254
    C:\Users\elion/.ssh/known_hosts:11: 10.3.2.12
    C:\Users\elion/.ssh/known_hosts:12: 10.5.1.11
    C:\Users\elion/.ssh/known_hosts:13: 10.5.1.254
    (12 additional names omitted)
Are you sure you want to continue connecting (yes/no/[fingerprint])?

[elio@john ~]$ sudo ssh-keygen -l -f /etc/ssh/ssh_host_ed25519_key
[sudo] password for elio:
256 SHA256:BpvSJEF2Xvw7TxoLxlzazSVYgSGV/O7U1Ufrr/xa45c /etc/ssh/ssh_host_ed25519_key.pub (ED25519)
```

🌞 Consulter l'état actuel

```
[elio@router ~]$ sudo ss -ltpnu
Netid       State        Recv-Q       Send-Q               Local Address:Port               Peer Address:Port       Process
udp         UNCONN       0            0                        127.0.0.1:323                     0.0.0.0:*           users:(("chronyd",pid=681,fd=5))
udp         UNCONN       0            0                            [::1]:323                        [::]:*           users:(("chronyd",pid=681,fd=6))
tcp         LISTEN       0            128                        0.0.0.0:22                      0.0.0.0:*           users:(("sshd",pid=693,fd=3))
tcp         LISTEN       0            128                           [::]:22                         [::]:*           users:(("sshd",pid=693,fd=4))
```

🌞 Modifier la configuration du serveur SSH

```
[elio@router ~]$ sudo vim /etc/ssh/sshd_config

[elio@router ~]$ sudo ss -tpn
[sudo] password for elio:
State  Recv-Q  Send-Q   Local Address:Port     Peer Address:Port   Process
ESTAB  0       36          10.7.1.254:25000        10.7.1.1:59438   users:(("sshd",pid=1437,fd=4),("sshd",pid=1434,fd=4))
```

🌞 Prouvez que le changement a pris effet

```
[elio@john ~]$ sudo ss -ltpn
State          Recv-Q         Send-Q                  Local Address:Port                   Peer Address:Port         Process
LISTEN         0              128                           0.0.0.0:22                          0.0.0.0:*             users:(("sshd",pid=688,fd=3))
LISTEN         0              128                              [::]:22 
```
🌞 Effectuer une connexion SSH sur le nouveau port

```
PS C:\Users\elion> ssh elio@rout7 -p 25000
Last login: Thu Nov 23 10:58:59 2023 from 10.7.1.1
[elio@router ~]$ sudo ss -tpn
```
🌞 Générer une paire de clés
+
🌞 Connectez-vous en SSH à la machine

```
PS C:\Users\elion> ls .\.ssh\id_rsa.pub


    Répertoire : C:\Users\elion\.ssh


Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
-a----        23/11/2023     10:10            738 id_rsa.pub

PS C:\Users\elion> ssh elio@john7
Last login: Fri Nov 24 09:21:09 2023
[elio@john ~]$
```

🌞 Supprimer les clés sur la machine router.tp7.b1

```
[elio@router ~]$ sudo !!
sudo rm -r /etc/ssh/ssh_host_*
[sudo] password for elio:
[elio@router ~]$ ls /etc/ssh/
moduli  ssh_config  ssh_config.d  sshd_config  sshd_config.d
```

🌞 Regénérez les clés sur la machine router.tp7.b1

```
[elio@router ~]$ sudo ssh-keygen -A
ssh-keygen: generating new host keys: RSA DSA ECDSA ED25519

```

🌞 Tentez une nouvelle connexion au serveur

```
PS C:\Users\elion> ssh elio@rout7 -p 25000
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@    WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!     @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
IT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!
Someone could be eavesdropping on you right now (man-in-the-middle attack)!
It is also possible that a host key has just been changed.
The fingerprint for the ED25519 key sent by the remote host is
SHA256:mh/Y9IYzKnzsEmM6A93yjPq5iikCS2D+9PFfVu0mWT4.
Please contact your system administrator.
Add correct host key in C:\\Users\\elion/.ssh/known_hosts to get rid of this message.
Offending RSA key in C:\\Users\\elion/.ssh/known_hosts:29
Host key for [rout7]:25000 has changed and you have requested strict checking.
Host key verification failed.
PS C:\Users\elion> ssh elio@rout7 -p 25000
The authenticity of host '[rout7]:25000 ([10.7.1.254]:25000)' can't be established.
ED25519 key fingerprint is SHA256:mh/Y9IYzKnzsEmM6A93yjPq5iikCS2D+9PFfVu0mWT4.
This key is not known by any other names
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '[rout7]:25000' (ED25519) to the list of known hosts.
Last login: Fri Nov 24 09:55:43 2023 from 10.7.1.1
[elio@router ~]$
```
🌞 Montrer sur quel port est disponible le serveur web

``` shell
sudo ss -ltpn
State    Recv-Q   Send-Q       Local Address:Port       Peer Address:Port   Process
LISTEN   0        128                0.0.0.0:22              0.0.0.0:*       users:(("sshd",pid=690,fd=3))
LISTEN   0        511                0.0.0.0:80              0.0.0.0:*       users:(("nginx",pid=11184,fd=6),("nginx",pid=11183,fd=6))
LISTEN   0        128                   [::]:22                 [::]:*       users:(("sshd",pid=690,fd=4))
LISTEN   0        511                   [::]:80                 [::]:*       users:(("nginx",pid=11184,fd=7),("nginx",pid=11183,fd=7))
```

🌞 Générer une clé et un certificat sur web.tp7.b1

```
[elio@web ~]$ openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout server.key -out server.crt
...+......+...+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*.+......+............+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*......+............+...+..+.+............+...+.....+......+......+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
..+...+.........+......+.+..............+.+..............+....+...+.....+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*..+.....+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*.+.......+......+.........+...+..+.+...+..............+.+..+....+...+..+......+...+.+...+..+.+.....+...................+...+......+...............+.....+......+....+............+..+.......+......+.....+.............+.........+...+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:fr
State or Province Name (full name) []:Nouvelle-Aquitaine
Locality Name (eg, city) [Default City]:Bordeaux
Organization Name (eg, company) [Default Company Ltd]:
Organizational Unit Name (eg, section) []:
Common Name (eg, your name or your server's hostname) []:
Email Address []:
[elio@web ~]$ sudo mv server.key /etc/pki/tls/private/web.tp7.b1.key
[sudo] password for elio:
Sorry, try again.
[sudo] password for elio:
[elio@web ~]$ sudo mv server.crt /etc/pki/tls/certs/web.tp7.b1.crt
[elio@web ~]$ sudo chown nginx:nginx /etc/pki/tls/private/web.tp7.b1.key
[elio@web ~]$ sudo chown nginx:nginx /etc/pki/tls/certs/web.tp7.b1.crt
[elio@web ~]$ sudo chmod 0400 /etc/pki/tls/private/web.tp7.b1.key
[elio@web ~]$ sudo chmod 0444 /etc/pki/tls/certs/web.tp7.b1.crt
```

🌞 Modification de la conf de NGINX

```shell
[elio@web ~]$ sudo cat /etc/nginx/conf.d/site_web_nul.conf
server {

    listen 10.7.1.12:443 ssl;

    
    ssl_certificate /etc/pki/tls/certs/web.tp7.b1.crt;
    ssl_certificate_key /etc/pki/tls/private/web.tp7.b1.key;

    server_name www.site_web_nul.b1;
    root /var/www/site_web_nul;

}
```

🌞 Conf firewall

```shell
[elio@web ~]$ sudo firewall-cmd --add-port=443/tcp --permanent
success
[elio@web ~]$ sudo firewall-cmd --reload
success
```

🌞 Redémarrez NGINX

```shell
[elio@web ~]$ sudo systemctl restart nginx
```
🌞 Prouvez que NGINX écoute sur le port 443/tcp

```shell
[elio@web ~]$ sudo ss -ltpn | grep 443
LISTEN 0      511        10.7.1.12:443       0.0.0.0:*    users:(("nginx",pid=11274,fd=6),("nginx",pid=11273,fd=6))
```

🌞 Visitez le site web en https
```shell
[elio@john ~]$ curl -k https://10.7.1.12
<h1>MEOW</h1>
```

