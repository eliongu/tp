**TP4**
- 
🌞 Déterminer
```
PS C:\Users\elion> ipconfig /all
   Bail obtenu. . . . . . . . . . . . . . : jeudi 9 novembre 2023 09:41:03
   Bail expirant. . . . . . . . . . . . . : vendredi 10 novembre 2023 09:04:33
   Passerelle par défaut. . . . . . . . . : 10.33.51.254
   Serveur DHCP . . . . . . . . . . . . . : 10.33.51.254
```
🌞 Capturer un échange DHCP

```
[Echange](./tp4_dhcp_client.pcapng)
```
