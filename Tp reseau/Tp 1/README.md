**Tp-1 Reseau** 
- 
I Exploration locale en solo
-

*Affichez les information des cartes réseau de son PC :*  
```
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : MediaTek Wi-Fi 6 MT7921 Wireless LAN Card
   Adresse physique . . . . . . . . . . . : 48-E7-DA-58-7C-03
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.48.121(préféré)
   (je n'ai pas de carte ethernet)    
```

***Affichez votre gateway***  

```Passerelle par défaut. . . . . . . . . :10.33.51.254 ```

 **L'Adresse MAC de la passerelle**
 ```10.33.51.254          7c-5a-1c-cb-fd-a4     dynamique```

 **Modification d'adresse IP**  
 ```Quand j'ai modifié mon adresse IP j'ai pris l'adresse de quelqu'un d'autre ce qui fait que les paquets envoyés ont 1 adresse pour 2 destinataires différents, donc rien n'est reçus. ```   

III Manipulations d'autres outils/protocoles côté client
-
**Exploration du DHCP, depuis votre PC**

 ``` 
 -ipconfig /all
  Bail expirant. . . . . . . . . . . . . : mardi 17 octobre 2023 09:01:38
 Serveur DHCP . . . . . . . . . . . . . : 10.33.51.254

 ```

**Trouver l'adresse IP du serveur DNS que connaît votre ordinateur** 

```  
Serveurs DNS. . .  . . . . . . . . . . : 10.33.10.2
                                       8.8.8.8

```
**nslookup**

```
nslookup google.com 8.8.8.8
Serveur :   dns.google
Address:  8.8.8.8
Réponse ne faisant pas autorité :
Nom :    google.com
Addresses:  2a00:1450:4007:818::200e
142.250.179.110        
PS C:\Users\elion> nslookup ynov.com 8.8.8.8
Serveur :   dns.google
Address:  8.8.8.8

Réponse ne faisant pas autorité :
Nom :    ynov.com
Addresses:  2606:4700:20::681a:be9
2606:4700:20::681a:ae9
2606:4700:20::ac43:4ae2
104.26.11.233
104.26.10.233
172.67.74.226
```