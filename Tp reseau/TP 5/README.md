**TP 5**
-

🌞 Déterminez, pour ces 5 applications, si c'est du TCP ou de l'UDP

[Echange avec Microsoft edge](./tp5_service_1.pcapng)

[Echange avec Discord](./tp5_service_2.pcapng)

[Echange avec Spotify](./tp5_service_3.pcapng)


```
  Proto  Adresse locale         Adresse distante       État
  TCP    10.33.70.198:7680      10.33.65.196:50554     TIME_WAIT
  TCP    10.33.70.198:7680      10.33.66.100:57788     TIME_WAIT
  TCP    10.33.70.198:49649     20.199.120.85:443      ESTABLISHED
 [Discord.exe]
  TCP    10.33.70.198:55550     172.65.251.78:443      TIME_WAIT
  TCP    10.33.70.198:55554     20.189.173.18:443      ESTABLISHED
 [msedge.exe]
  TCP    10.33.70.198:55559     35.186.224.25:443      ESTABLISHED
 [Spotify.exe]
  TCP    10.33.70.198:55576     104.26.11.240:443      TIME_WAIT
  TCP    10.33.70.198:55577     23.192.237.197:443     ESTABLISHED
```


🌞 Examinez le trafic dans Wireshark
Nous sommes en tcp, on reçoit des accusés de reception sur chaque messages envoyé

[Echange ssh en tcp début](./handshake.pcapng)


[Echange ssh en tcp fin](./Fin_ack.pcapng)


🌞 Demandez aux OS
```
PS C:\Users\elion> netstat -n -b | Select-String "ssh" -Context 1,0

    TCP    10.5.1.1:55746         10.5.1.11:22           ESTABLISHED
>  [ssh.exe]

```

```
[elio@localhost ~]$ ss -tpn
State  Recv-Q  Send-Q   Local Address:Port     Peer Address:Port   Process
ESTAB  0       52           10.5.1.11:22           10.5.1.1:55746
```
[Capture complète](./tp5_3_way.pcapng)

🌞 Prouvez que

```
[elio@localhost ~]$ ip a
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:99:2b:cb brd ff:ff:ff:ff:ff:ff
    inet 10.5.1.11/24 brd 10.5.1.255 scope global noprefixroute enp0s3
[elio@localhost ~]$ ping ynov.com
PING ynov.com (172.67.74.226) 56(84) bytes of data.
64 bytes from 172.67.74.226 (172.67.74.226): icmp_seq=1 ttl=55 time=21.5 ms
64 bytes from 172.67.74.226 (172.67.74.226): icmp_seq=2 ttl=55 time=21.7 ms
^C64 bytes from 172.67.74.226: icmp_seq=3 ttl=55 time=21.9 ms

--- ynov.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 10115ms
rtt min/avg/max/mdev = 21.537/21.718/21.871/0.137 ms
```

🌞 Installez le paquet nginx

```
[elio@localhost ~]$ sudo dnf install nginx
Rocky Linux 9 - BaseOS                      402  B/s | 4.1 kB     00:10
...
```

🌞 Créer le site web

```
[elio@localhost ~]$ sudo mkdir -p /var/www/site_web_nul/

[elio@localhost site_web_nul]$ sudo nano index.html
[elio@localhost site_web_nul]$ cat index.html
<h1>MEOW</h1>
```

🌞 Donner les bonnes permissions

```
[elio@localhost site_web_nul]$ sudo chown -R nginx:nginx /var/www/site_web_nul
```

🌞 Créer un fichier de configuration NGINX pour notre site web

```
[elio@localhost ~]$ sudo touch /etc/nginx/conf.d/site_web_nul.conf
[elio@localhost ~]$ sudo cat /etc/nginx/conf.d/site_web_nul.conf
[sudo] password for elio:
server {
  listen 80;

  index index.html;

  server_name www.site_web_nul.b1;

  root /var/www/site_web_nul;
}
```

🌞 Démarrer le serveur web !

```
[elio@localhost conf.d]$ sudo nano site_web_nul.conf
[sudo] password for elio:
[elio@localhost conf.d]$ sudo systemctl start nginx
[sudo] password for elio:
[elio@localhost conf.d]$ systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; prese>
     Active: active (running) since Thu 2023-11-16 10:13:19 CET; 17s ago
```

🌞 Ouvrir le port firewall

```
[elio@localhost conf.d]$ sudo firewall-cmd --add-port=80/tcp --permanent
[sudo] password for elio:
success
[elio@localhost conf.d]$ sudo firewall-cmd --reload
success
```

🌞 Visitez le serveur web !

```
[elio@localhost ~]$ curl 10.5.1.12
<h1>MEOW</h1>

```

🌞 Visualiser le port en écoute

```
[elio@localhost conf.d]$ ss -tpn
State  Recv-Q  Send-Q   Local Address:Port     Peer Address:Port   Process
ESTAB  0       0            10.5.1.12:80           10.5.1.1:62413
ESTAB  0       0            10.5.1.12:22           10.5.1.1:62242
ESTAB  0       0            10.5.1.12:80           10.5.1.1:62412
```
