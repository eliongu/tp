**TP-2**
-

🌞 Mettez en place une configuration réseau fonctionnelle entre les deux machines
``` 
[elio@localhost ~]$ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:29:67:e6 brd ff:ff:ff:ff:ff:ff
    inet *10.3.1.13/24* brd 10.3.1.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe29:67e6/64 scope link
       valid_lft forever preferred_lft forever
```
Pour la premiere machine et l'ip de la 2eme est 10.3.1.14

🌞Prouvez que la connexion est fonctionnelle entre les deux machines
```
[elio@localhost ~]$ ping 10.3.1.14
PING 10.3.1.14 (10.3.1.14) 56(84) bytes of data.
64 bytes from 10.3.1.14: icmp_seq=1 ttl=64 time=0.842 ms
64 bytes from 10.3.1.14: icmp_seq=2 ttl=64 time=1.13 ms
64 bytes from 10.3.1.14: icmp_seq=3 ttl=64 time=1.20 ms
64 bytes from 10.3.1.14: icmp_seq=4 ttl=64 time=1.36 ms
^C
--- 10.3.1.14 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3049ms
rtt min/avg/max/mdev = 0.842/1.132/1.360/0.187 ms
```

🌞Check the ARP table

```
[elio@localhost ~]$ ip neigh show
10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:04 (MAC Passerelle) REACHABLE
10.3.1.14 dev enp0s3 lladdr 08:00:27:ac:64:8b (MAC PC2) STALE

```
🌞 Manipuler la table ARP

```
[elio@localhost ~]$ sudo ip neigh flush all
[elio@localhost ~]$ ip neigh show
10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:04 REACHABLE
--====--
10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:04 REACHABLE
10.3.1.14 dev enp0s3 lladdr 08:00:27:ac:64:8b REACHABLE
[elio@localhost ~]$
```
