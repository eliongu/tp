**Tp 6**
-

🌞 Dans le rendu, je veux

```
[elio@dns ~]$ sudo cat /etc/named.conf
//
// named.conf
//
// Provided by Red Hat bind package to configure the ISC BIND named(8) DNS
// server as a caching only nameserver (as a localhost DNS resolver only).
//
// See /usr/share/doc/bind*/sample/ for example named configuration files.
//

options {
        listen-on port 53 { 127.0.0.1; any; };
        listen-on-v6 port 53 { ::1; };
        directory       "/var/named";
        dump-file       "/var/named/data/cache_dump.db";
        statistics-file "/var/named/data/named_stats.txt";
        memstatistics-file "/var/named/data/named_mem_stats.txt";
        secroots-file   "/var/named/data/named.secroots";
        recursing-file  "/var/named/data/named.recursing";
        allow-query     { localhost; any; };
        allow-query-cache { localhost; any; };

        /*
         - If you are building an AUTHORITATIVE DNS server, do NOT enable recursion.
         - If you are building a RECURSIVE (caching) DNS server, you need to enable
           recursion.
         - If your recursive DNS server has a public IP address, you MUST enable access
           control to limit queries to your legitimate users. Failing to do so will
           cause your server to become part of large scale DNS amplification

           attacks. Implementing BCP38 within your network would greatly
           reduce such attack surface
        */
        recursion yes;

        dnssec-validation yes;

        managed-keys-directory "/var/named/dynamic";
        geoip-directory "/usr/share/GeoIP";

        pid-file "/run/named/named.pid";
        session-keyfile "/run/named/session.key";

        /* https://fedoraproject.org/wiki/Changes/CryptoPolicy */
        include "/etc/crypto-policies/back-ends/bind.config";
};

logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};

zone "." IN {
        type hint;
        file "named.ca";
};

include "/etc/named.rfc1912.zones";
include "/etc/named.root.key";

zone "tp6.b1" IN {
     type master;
     file "tp6.b1.db";
     allow-update { none; };
     allow-query {any; };
};

zone "1.4.10.in-addr.arpa" IN {
     type master;
     file "tp6.b1.rev";
     allow-update { none; };
     allow-query { any; };
};
[elio@dns ~]$ sudo cat /var/named/tp6.b1.db
$TTL 86400
@ IN SOA dns.tp6.b1. admin.tp6.b1. (
    2019061800 ;Serial
    3600 ;Refresh
    1800 ;Retry
    604800 ;Expire
    86400 ;Minimum TTL
)

; Infos sur le serveur DNS lui même (NS = NameServer)
@ IN NS dns.tp6.b1.

; Enregistrements DNS pour faire correspondre des noms à des IPs
dns       IN A 10.6.1.101
john      IN A 10.6.1.11
[elio@dns ~]$  sudo cat /var/named/tp6.b1.rev
$TTL 86400
@ IN SOA dns.tp6.b1. admin.tp6.b1. (
    2019061800 ;Serial
    3600 ;Refresh
    1800 ;Retry
    604800 ;Expire
    86400 ;Minimum TTL
)

; Infos sur le serveur DNS lui même (NS = NameServer)
@ IN NS dns.tp6.b1.

; Reverse lookup
101 IN PTR dns.tp6.b1.
11 IN PTR john.tp6.b1.
```

```
[elio@dns ~]$ systemctl status named
● named.service - Berkeley Internet Name Domain (DNS)
     Loaded: loaded (/usr/lib/systemd/system/named.service; enabled; preset>
     Active: active (running) since Fri 2023-11-17 11:55:43 CET; 5min ago
   Main PID: 2557 (named)
      Tasks: 5 (limit: 2681)
     Memory: 16.2M
        CPU: 83ms
     CGroup: /system.slice/named.service
             └─2557 /usr/sbin/named -u named -c /etc/named.conf

Nov 17 11:55:43 dns.tp1.b1 named[2557]: network unreachable resolving './NS>
Nov 17 11:55:43 dns.tp1.b1 named[2557]: zone localhost/IN: loaded serial 0
Nov 17 11:55:43 dns.tp1.b1 named[2557]: zone 1.0.0.127.in-addr.arpa/IN: loa>
Nov 17 11:55:43 dns.tp1.b1 named[2557]: zone tp6.b1/IN: loaded serial 20190>
Nov 17 11:55:43 dns.tp1.b1 named[2557]: zone localhost.localdomain/IN: load>
Nov 17 11:55:43 dns.tp1.b1 named[2557]: all zones loaded
Nov 17 11:55:43 dns.tp1.b1 systemd[1]: Started Berkeley Internet Name Domai>
Nov 17 11:55:43 dns.tp1.b1 named[2557]: running
Nov 17 11:55:43 dns.tp1.b1 named[2557]: managed-keys-zone: Initializing aut>
Nov 17 11:55:43 dns.tp1.b1 named[2557]: resolver priming query complete
lines 1-20/20 (END)

```
```
[elio@dns ~]$ ss -tulpn
Netid State  Recv-Q Send-Q   Local Address:Port   Peer Address:Port Process
udp   UNCONN 0      0           10.6.1.101:53          0.0.0.0:*
udp   UNCONN 0      0            127.0.0.1:53          0.0.0.0:*
udp   UNCONN 0      0            127.0.0.1:323         0.0.0.0:*
udp   UNCONN 0      0                [::1]:53             [::]:*
udp   UNCONN 0      0                [::1]:323            [::]:*
tcp   LISTEN 0      4096         127.0.0.1:953         0.0.0.0:*
tcp   LISTEN 0      128            0.0.0.0:22          0.0.0.0:*
tcp   LISTEN 0      10          10.6.1.101:53          0.0.0.0:*
tcp   LISTEN 0      10           127.0.0.1:53          0.0.0.0:*
tcp   LISTEN 0      10               [::1]:53             [::]:*
tcp   LISTEN 0      128               [::]:22             [::]:*
tcp   LISTEN 0      4096             [::1]:953            [::]:*
```

🌞 Ouvrez le bon port dans le firewall

```
[elio@dns ~]$ sudo firewall-cmd --add-port=53/tcp --permanent
[sudo] password for elio:
success
[elio@dns ~]$ sudo firewall-cmd --reload
success
```
🌞 Sur la machine john.tp6.b1

```
[elio@john ~]$ ping john.tp6.b1
PING john.tp6.b1(john.tp6.b1 (fe80::a00:27ff:febe:356%enp0s3)) 56 data bytes
64 bytes from john.tp6.b1 (fe80::a00:27ff:febe:356%enp0s3): icmp_seq=1 ttl=64 time=0.098 ms
64 bytes from john.tp6.b1 (fe80::a00:27ff:febe:356%enp0s3): icmp_seq=2 ttl=64 time=0.117 ms
^C64 bytes from fe80::a00:27ff:febe:356%enp0s3: icmp_seq=3 ttl=64 time=0.096 ms

--- john.tp6.b1 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 0.096/0.103/0.117/0.009 ms

[elio@john ~]$ ping dns.tp6.b1
ping: dns.tp6.b1: Name or service not known
[elio@john ~]$ ping dns.tp6.b1
PING dns.tp6.b1 (10.6.1.101) 56(84) bytes of data.
64 bytes from 10.6.1.101 (10.6.1.101): icmp_seq=1 ttl=64 time=0.290 ms
64 bytes from 10.6.1.101 (10.6.1.101): icmp_seq=2 ttl=64 time=1.12 ms
^C
--- dns.tp6.b1 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 0.290/0.706/1.123/0.416 ms

[elio@john ~]$ ping www.ynov.com
PING www.ynov.com (104.26.11.233) 56(84) bytes of data.
64 bytes from 104.26.11.233 (104.26.11.233): icmp_seq=1 ttl=54 time=13.7 ms
64 bytes from 104.26.11.233 (104.26.11.233): icmp_seq=2 ttl=54 time=22.3 ms
64 bytes from 104.26.11.233 (104.26.11.233): icmp_seq=3 ttl=54 time=23.6 ms
64 bytes from 104.26.11.233 (104.26.11.233): icmp_seq=4 ttl=54 time=14.1 ms
^C
--- www.ynov.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 13.733/18.446/23.593/4.545 ms
```
🌞 Sur votre PC

```
PS C:\Users\elion> ping john.tp6.b1

Envoi d’une requête 'ping' sur toto [10.6.1.11] avec 32 octets de données :
Réponse de 10.6.1.11 : octets=32 temps<1ms TTL=64
Réponse de 10.6.1.11 : octets=32 temps<1ms TTL=64
Réponse de 10.6.1.11 : octets=32 temps<1ms TTL=64
Réponse de 10.6.1.11 : octets=32 temps<1ms TTL=64

Statistiques Ping pour 10.6.1.11:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
```
